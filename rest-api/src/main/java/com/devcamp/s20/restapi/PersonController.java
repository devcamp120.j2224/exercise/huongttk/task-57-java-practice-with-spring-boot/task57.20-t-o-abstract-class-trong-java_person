package com.devcamp.s20.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonController {
    // Tạo Api có tên listStudent trả ra danh sách Student
    @CrossOrigin
    @GetMapping("/listStudent")
    public ArrayList<Student> getListStudent(){
        ArrayList<Student> listStudent = new ArrayList<Student>();

        Address address = new Address("Pho Quang", "Ho Chi Minh", "Viet Nam", 1204);
        Address address1 = new Address("Ngo Quyen", "Phan Rang", "Viet Nam", 84);
        Professo teacher1 = new Professo(35, "male", "An", address);
        Professo teacher2 = new Professo(28, "female", "Ms kim", address1);
        Subject subject1 = new Subject("Hoa hoc", 1, teacher1);
        Subject subject2 = new Subject("English", 3, teacher2);
        ArrayList<Subject> listSubject = new ArrayList<Subject>();
        listSubject.add(subject1);
        listSubject.add(subject2);

        Student student1 = new Student(15, "female", "Huong", address1 , 11, listSubject);
        Student student2 = new Student(16, "male", "Duy", address , 12, listSubject);
        Student student3 = new Student(17, "female", "Tiet", address1 , 13, listSubject);
        Student student4 = new Student(12, "male", "Phomai", address , 14, listSubject);

        listStudent.add(student1);
        listStudent.add(student2);
        listStudent.add(student3);
        listStudent.add(student4);

        return listStudent;
    }
}
