package com.devcamp.s20.restapi;



public class Subject {
    
    private String subTitle;
    private int subId;
    private Professo teacher;
    
    public Subject(String subTitle, int subId, Professo teacher) {
        this.subTitle = subTitle;
        this.subId = subId;
        this.teacher = teacher;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public int getSubId() {
        return subId;
    }

    public void setSubId(int subId) {
        this.subId = subId;
    }

    public Professo getTeacher() {
        return teacher;
    }

    public void setTeacher(Professo teacher) {
        this.teacher = teacher;
    }
   
    
}
