package com.devcamp.s20.restapi;

public class Professo extends Person {
    private int salary;

    public Professo(int age, String gender, String name, Address address) {
        super(age, gender, name, address);
    }

    public Professo(int age, String gender, String name, Address address, int salary) {
        super(age, gender, name, address);
        this.salary = salary;
    }

    @Override
    public void eat() {
        // TODO Auto-generated method stub
        System.out.println("Professor is eating...");
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
    
   public String teaching(){
    return new String ("Professor is teaching...");
   }
}
